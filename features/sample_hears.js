/**
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 */
module.exports = function(controller) {

    // use a function to match a condition in the message
    controller.hears(async(message) => message.text && message.text.toLowerCase() === 'foo', ['message'], async (bot, message) => {
        await bot.reply(message, 'I heard "foo" via a function test');
    });

    // use a regular expression to match the text of the message
    controller.hears(new RegExp(/^\d+$/), ['message','direct_message'], async function(bot, message) {
        await bot.reply(message,{ text: 'I heard a number using a regular expression.' });
    });

    // match any one of set of mixed patterns like a string, a regular expression
    controller.hears(['allcaps', new RegExp(/^[A-Z\s]+$/)], ['message','direct_message'], async function(bot, message) {
        await bot.reply(message,{ text: 'I HEARD ALL CAPS!' });
    });

    controller.hears(async(message) => {return message.welcomeMessage }, 'message', async(bot, message) => { 
        await bot.reply(message,{ text: 'Hi there' });
    });
    controller.hears(async(message) => { return message.form_data }, 'message', async(bot, message) => { 
        
        const form_field_name = message.form_field_name;
        const form_field_value = message.form_field_value;
        const answer = `The form field ${form_field_name} has been changed to: "${form_field_value}"`
        await bot.reply(message,{ text: answer , set_form_field:true,  form_field_name:form_field_name, form_field_value:'pizda'});
    });

}