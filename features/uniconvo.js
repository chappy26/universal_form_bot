const { BotkitConversation } = require('botkit');
const debug = require('debug')('UNIFORM');
const UNI_DIALOG_ID = 'universal-dialog';
const _ = require('lodash')
const initialForm = require('../data/form')

const createDialog = (controller) => {
    const convo = new BotkitConversation(UNI_DIALOG_ID, controller);
    const unitext = async (template, vars) => {
        return vars.to_ask.prompt
    }
    const uniskipper = async (template, vars) => {
        return [{ title: 'Skip question', payload: "skip" }]
    }
    const nextQ = (el, form) => {
        const ind = _.findIndex(form, { 'var': el.var });
        let r = _.chain(form).slice(ind + 1).find(i => i.value === undefined && i.var != el.var).value();
        if (!r) { r = _.chain(form).slice({ end: ind - 1 }).find(i => i.value === undefined && i.var != el.var).value(); }
        return r;
    }
    const skippingCallback = async (answer, convo, bot) => {
        const to_ask = convo.vars.to_ask;
        const form = convo.vars.form;
        const next_q = nextQ(to_ask, form);
        if (next_q) { convo.setVar('to_ask', next_q) } else {
            await bot.say('Sorry, that is the last question, try to answer it!')
        }
        convo.gotoThread('default')
    }
    const unicallback = async (answer, convo, bot) => {
        const to_ask = convo.vars.to_ask;
        const form = convo.vars.form;
        const validator = _.chain(initialForm).find({ var: to_ask.var }).value().validator
        if (validator) {
            const validatorResponse = validator(answer)
            if (validatorResponse) {
                await bot.say(validatorResponse);
                return await convo.repeat();
            }
        }
        const botAnswer = `Setting field ${to_ask.var} to "${answer}"`;
        await bot.say({ text: botAnswer, set_form_field: true, form_field_name: to_ask.var, form_field_value: answer });
        const newForm = _.map(form, function (a) {
            return a.var === to_ask.var ? { ...a, value: answer } : a;
        });
        convo.setVar('form', newForm);
        const next_q = nextQ(to_ask, form);
        if (next_q) {
            convo.setVar('to_ask', next_q);
            convo.gotoThread('default');
        } else { await bot.say('Goodbye!') }
    }

    convo.before('default', async (convo, bot) => {
        if (convo.vars.to_ask === undefined) {
            const form = convo.vars.form;
            const unfilled = _.find(form, i => i.value === undefined);
            convo.setVar('to_ask', unfilled);
        }
    });

    convo.ask(message = { text: unitext, quick_replies: uniskipper },
        handlers = [
            { pattern: 'skip', handler: skippingCallback },
            { default: true, handler: unicallback }
        ],
        key = 'temp');
    controller.addDialog(convo);
}

module.exports = function (controller) {
    createDialog(controller);
    controller.hears('test', 'message', async (bot, message) => {
        await bot.beginDialog(UNI_DIALOG_ID, { form: _.cloneDeep(initialForm) })
    });
    
}
