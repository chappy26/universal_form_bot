const ageVal = (val) => {
    const v = parseInt(val)
    if (!Number.isInteger(v)) return 'You should enter number here'
    if (v < 18) return 'You should be older than 18'
    if (v > 100) return 'It seems you are too old'
}
module.exports = [
    { var: "age", prompt: "How  old are you?", validator: ageVal },
    { var: "income", prompt: "How much do you earn per month?" },
    { var: "name", prompt: "What is your name?" },
    { var: "plz", prompt: "What is postal index?"}
]
